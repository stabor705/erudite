package pl.edu.agh.kis.erudite.edition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/editions")
@CrossOrigin(origins = "http://localhost:3000")
public class EditionController {
    private final EditionService service;

    @Autowired
    public EditionController(EditionService service) {
        this.service = service;
    }

    @GetMapping
    public Iterable<Edition> getAllEditions() {
        return service.getAllEditions();
    }

    @GetMapping("/{id}")
    public Edition getEdition(@PathVariable Long id) {
        return service.getEdition(id);
    }

    @GetMapping("/search")
    public List<Edition> searchEdition(@RequestParam(name = "title", required = false) String title,
                                       @RequestParam(name = "author", required = false) String author,
                                       @RequestParam(name = "isbn", required = false) String isbn) {
        return service.searchEditions(title, author, isbn);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Edition edition) {
        service.addEdition(edition);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        service.deleteEdition(id);
    }
}
