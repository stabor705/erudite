package pl.edu.agh.kis.erudite.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

@Service
public class AuthorService {
    private AuthorRepository repository;
    @Autowired
    public AuthorService(AuthorRepository repository) {
        this.repository = repository;
        /**
        Author author = new Author();
        author.setFirstName("Jan");
        author.setSurname("Kowalski");
        repository.save(author);
         **/
    }

    public Author getAuthor(Long id) {
        return repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public Author addAuthor(Author author) {
        return repository.save(author);
    }

    public void deleteAuthor(Long id) {
        repository.deleteById(id);
    }
}
