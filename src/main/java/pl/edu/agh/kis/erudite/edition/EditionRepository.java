package pl.edu.agh.kis.erudite.edition;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EditionRepository extends JpaRepository<Edition, Long> {
    @Query("SELECT e " +
            "FROM Edition e JOIN e.authors a " +
            "WHERE UPPER(e.book.title) LIKE %?1% AND " +
            "UPPER(a.name) LIKE %?2% AND " +
            "(e.isbn = ?3 OR ?3 IS NULL or ?3 = '')")
    List<Edition> findByTitleAuthorAndIsbn(String upperTitle, String upperAuthor, String isbn);
}
