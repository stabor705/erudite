package pl.edu.agh.kis.erudite.publisher;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.net.URL;
import java.util.Objects;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "publishers")
public class Publisher {
    @Column(name = "id_publisher")
    private @Id
    @GeneratedValue Long id;
    private String name;
    private String country;
    private String description;

    private String logo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publisher publisher = (Publisher) o;
        return Objects.equals(id, publisher.id) && Objects.equals(name, publisher.name) && Objects.equals(country, publisher.country) &&
                 Objects.equals(description, publisher.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, country, description);
    }
}

